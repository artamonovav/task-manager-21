package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

}
