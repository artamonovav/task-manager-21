package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.api.repository.IRepository;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
