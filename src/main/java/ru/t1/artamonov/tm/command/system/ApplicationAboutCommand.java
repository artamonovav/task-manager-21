package ru.t1.artamonov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "Display developer info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name:    Anatoly Artamonov");
        System.out.println("E-mail:  aartamonov@t1-consulting.ru");
    }

}
